import axios from 'axios'

export const peopleList = {
    data: () => ({
        people: [],
        search: '',
        favoriteList: [],
        loading: false,
        currentGender: 'все',
        genders: [
            'все',
            'муж',
            'жен',
            'н/д'
        ],
        pageSize: 4
    }),
    mounted() {
        this.getPeopleApi()
        this.resizePage()
        window.onresize = () => {
            this.resizePage()
        }
    },
    computed: {
        filteredPeople: ({ people, search, currentGender }) => {
            let nameFilteredPeopleArr
            nameFilteredPeopleArr = people.filter(item => {
                if (currentGender === 'муж') {
                    return item.gender === 'male'
                } else if (currentGender === 'жен') {
                    return item.gender === 'female'
                } else if (currentGender === 'н/д') {
                    return item.gender === 'n/a'
                } else return true
            }).filter(item =>
                item.name.toLowerCase()
                    .indexOf(search.toLowerCase()) !== -1)
            return nameFilteredPeopleArr
        },
        favoritePeople: ({ filteredPeople, favoriteList }) => {
            return filteredPeople.filter(item => {
                return favoriteList.indexOf(item.name) !== -1
            })
        }
    },
    watch: {
        people: function () {
            this.createFavoriteList()
        }
    },
    methods: {
        resizePage () {
            const width = window.innerWidth
            if (width >= 1200) {
                this.pageSize = 4
            } else if (width < 1200 && width >= 991) {
                this.pageSize = 3
            } else if (width < 991 && width >= 576) {
                this.pageSize = 2
            } else if (width < 576) {
                this.pageSize = 1
            }
            this.currentPage = 1
            this.$forceUpdate()
        },
        changePage (value) {
            this.currentPage = value
        },
        changeCurrentGender (value) {
            this.currentGender = value
        },
        likeCharacter (name) {
            if (this.favoriteList.indexOf(name) === -1) {
                localStorage.setItem(name, 'true')
            } else {
                localStorage.removeItem(name)
                this.favoriteList.splice(this.favoriteList.indexOf(name), 1)
            }
            this.createFavoriteList ()
        },
        createFavoriteList () {
            this.favoriteList = []
            this.people.forEach(item => {
                if (localStorage.getItem(item.name)) {
                    this.favoriteList.push(item.name)
                }
            })
        },
        isFavorite: (name, favoriteList) => favoriteList.indexOf(name) !== -1,
        indexPeople (item) {
            return item && +item.url.slice(28,30).replace('/', '') - 1
        },
        getPeopleApi () {
            this.loading = true
            axios({
                method: 'get',
                url: `https://swapi.co/api/people/`
            })
                .then(response => {
                    this.loading = false
                    this.people = response.data.results
                    response.data.results.forEach((item, i) => {
                        this.people[i].homeworldLoading = true
                        axios({
                            method: 'get',
                            url: item.homeworld
                        })
                            .then(response => {
                                this.people[i].homeworldName = response.data.name
                                this.people[i].homeworldLoading = false
                                this.people = [ ...this.people ]
                            })
                            .catch(response => {
                                console.log(response)
                            })
                    })
                })
                .catch(response => {
                    console.log(response)
                })
        }
    }
}